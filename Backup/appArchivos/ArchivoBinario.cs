﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace appArchivos
{
    public class ArchivoBinario : Archivo
    {
        public ArchivoBinario(string pRuta): base(pRuta) 
        {
        }

        public override string leer(ref string pError)
        {
            pError = "";
            string lineas = "";              
            try
            {
                FileStream stream = new FileStream(this.Ruta, FileMode.Open, FileAccess.Read);
                BinaryReader reader = new BinaryReader(stream);                                                
                while (reader.PeekChar() > -1) lineas += reader.ReadString() + "\n";                
                reader.Close();
                stream.Close();
            }
            catch (Exception ex)
            {
                pError = "Ha ocurrido un error" +
                         "\nDetalle técnico:" +
                         ex.Message;
            }
            return lineas;
        }

        public override string escribir(string pTexto)
        {
            string error = "";
            try
            {
                FileStream stream = new FileStream(this.Ruta, FileMode.Append);
                BinaryWriter writer = new BinaryWriter(stream, Encoding.UTF8);
                writer.Write(pTexto);                
                writer.Close();
            }
            catch (Exception ex)
            {
                error = "Ha ocurrido un error" +
                         "\nDetalle técnico:" +
                         ex.Message;
            }
            return error;
        }

        public override string crear()
        {
            string error = "";
            try
            {
                FileStream stream = new FileStream(this.Ruta, FileMode.Create);
                stream.Close();
            }
            catch (Exception ex)
            {
                error = "Ha ocurrido un error" +
                         "\nDetalle técnico:" +
                         ex.Message;
            }
            return error;
        }        
    }
}
